<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HotelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * if admin with id = 1 then admin accessible for creating hotels.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        if (Auth::user()->id == 1){
            return view('admin.hotel.create');
        } else {
            return redirect()->back();
        }
    }

    /**
     * storing the data in the database.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Hotel::create($request->all());
        return redirect()->back();
    }



}
