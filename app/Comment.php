<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
protected  $fillable =['comment','hotel_id','user_id'];


    /**
     * inverse relationship between Comment table and Hotel table.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * inverse relationship between Comment table and  Users table.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
