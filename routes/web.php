<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

/**
 *  Route for creating Hotels
 */
Route::get('/admin','HotelsController@create')->name('admin.create');

/**
 *  Route for store Hotels in  the database.
 */

Route::post('/admin','HotelsController@store')->name('admin.store');

/**
 *  this route show all Hotels.
 */

Route::get('/', function(){

        $hotels =  \App\Hotel::all();
        return view('admin.hotel.index', compact('hotels'));

});

/**
 *  resource route for HotelsCommentController.
 */
Route::resource('/hotel','HotelsCommentController');

/**
 *  Route showing the Comments.
 */

Route::get('hotel/{id}','HotelsCommentController@show')->name('hotel.show');

/**
 *  Route for store Comments in  the database.
 */
Route::get('hotel/','HotelsCommentController@store')->name('hotel.store');




