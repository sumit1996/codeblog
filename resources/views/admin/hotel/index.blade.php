@extends('layouts.app')


@section('content')


    <h1>Hotels</h1>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Show Comments</th>
        </tr>
        </thead>
        <tbody>
        @if($hotels)
            @foreach($hotels as $hotel)
                <tr>
                    <td>{{$hotel['id']}}</td>
                    <td>{{$hotel['name']}}</td>
                    <td>{{$hotel->created_at->diffforhumans()}}</td>
                    <td>{{$hotel->updated_at->diffforhumans()}}</td>
                    <td><a href="{{route('hotel.show', $hotel->id)}}">Show Comment</a></td>
                </tr>
            @endforeach
        @endif

        </tbody>
    </table>

@stop