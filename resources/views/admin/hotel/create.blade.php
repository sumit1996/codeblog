
@extends('layouts.app')


@section('content')
    {!! Form::open(['method'=>'POST', 'action'=>'HotelsController@store']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Submit',  ['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop