


@extends('layouts.app')

@section('content')

                    <div class="panel-heading">
                        <h3>{{ ucfirst($hotel->name) }}</h3>
                    </div>

                        <h3>Comments</h3>
                        @foreach($hotel->comments as $comment)
                            <p><b>{{ $comment->user->name }}</b>: {!! $comment->comment !!}</p>
                        @endforeach

                        @if(Auth::check())

                            {!! Form::open(['method'=>'POST', 'action'=>'HotelsCommentController@store']) !!}

                            <input type="hidden" name="hotel_id" value="{{ $hotel->id }}">

                            <div class="form-group">
                                {!! Form::label('comment', 'Comment:') !!}
                                {!! Form::textarea('comment', null,['class'=>'form-control col-sm-6']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Submit',  ['class'=>'btn btn-primary']) !!}
                            </div>
                           {!! Form::close() !!}
                    @endif


@stop